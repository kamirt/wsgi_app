import sqlite3
import os



class DBControl(object):
    """
    Class that creates db file and initiate database if not exists
    also makes a simple function to execute commands through requests
    """
    def __init__(self, db_name):
        self.db_name = db_name
        self.init_database = self._create_database(db_name)


    def _create_database(self, db_name):
        if os.access(self.db_name, os.F_OK) == True:
            print 'db_file exists'
            connection = sqlite3.connect(self.db_name)
        else:
            print 'there is no db_file, creating...'
            connection = sqlite3.connect(self.db_name)
            cursor = connection.cursor()
            with open('sql_commands/createdb.sql', 'r') as creating:
                for command in creating:
                    cursor.executescript(command)
            connection.close()

    def execute_command(self, command, params=None):
        connection = sqlite3.connect(self.db_name)
        cursor = connection.cursor()
        if not params:
            cursor.execute(command)
        else:
            cursor.execute(command, params)
        connection.commit()
        result = cursor.fetchall()
        connection.close()
        return result