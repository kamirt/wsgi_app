BEGIN TRANSACTION;
CREATE TABLE area (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,name TEXT NOT NULL);
CREATE TABLE city (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,name TEXT NOT NULL,area_id INTEGER,FOREIGN KEY(area_id) REFERENCES areas(id));
CREATE TABLE comment (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,first_name TEXT NOT NULL,last_name TEXT NOT NULL,comment TEXT NOT NULL,city_id INTEGER,phone TEXT,email TEXT,FOREIGN KEY(city_id) REFERENCES cities(id));
INSERT INTO area VALUES(NULL, 'Краснодарский край');
INSERT INTO area VALUES(NULL, 'Ростовская область');
INSERT INTO area VALUES(NULL, 'Ставропольский край');
INSERT INTO city VALUES(NULL, 'Краснодар', (SELECT id FROM area WHERE area.name = 'Краснодарский край'));
INSERT INTO city VALUES(NULL, 'Кропоткин', (SELECT id FROM area WHERE area.name = 'Краснодарский край'));
INSERT INTO city VALUES(NULL, 'Славянск', (SELECT id FROM area WHERE area.name = 'Краснодарский край'));
INSERT INTO city VALUES(NULL, 'Ростов', (SELECT id FROM area WHERE area.name = 'Ростовская область'));
INSERT INTO city VALUES(NULL, 'Шахты', (SELECT id FROM area WHERE area.name = 'Ростовская область'));
INSERT INTO city VALUES(NULL, 'Батайск', (SELECT id FROM area WHERE area.name = 'Ростовская область'));
INSERT INTO city VALUES(NULL, 'Ставрополь', (SELECT id FROM area WHERE area.name = 'Ставропольский край'));
INSERT INTO city VALUES(NULL, 'Пятигорск', (SELECT id FROM area WHERE area.name = 'Ставропольский край'));
INSERT INTO city VALUES(NULL, 'Кисловодск', (SELECT id FROM area WHERE area.name = 'Ставропольский край'));
COMMIT;

