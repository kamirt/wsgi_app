# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from wsgi import App
from Cookie import SimpleCookie
import settings
import sqlite3
import json

from db import DBControl
app = App()



# home function
@app.route('/')
def home(request):
    context = {}
    context['title'] = 'homepage'
    context['hello_world'] = 'This is the home page of project'
    return context, {}, 'templates/home.html'


# form view
@app.route('/comment/')
def comments(request):
    command = 'select id, name from area;'
    sql_result = app.db.execute_command(command)
    data = ['<option value=0>Выбрать...</option>']
    for item in sql_result:
        data.append('<option value="' + str(item[0]) + '">' + item[1] + '</option>')
    

    context = {}
    context['areas'] = ''.join(data)
    context['title'] = 'Leave your comment'
    return context, {}, 'templates/form.html'


# receives comments
@app.route('/receive-comment', method='POST')
def comment_receiver(request):
    form = json.loads(request.data)
    fname = form['firstName']
    lname = form['lastName']
    comment = form['comment']
    phone = form.get('phone', '')
    email = form.get('email', '')
    city = form.get('city', '')

    command = 'insert into comment values (NULL, ?, ?, ?, ?, ?, ?)'
    sql_result = app.db.execute_command(command, params=(fname, lname, comment, city, phone, email,))

    return json.dumps({'success': True}), {'Content-Type':'application/json'}

# removes comments
@app.route('/remove-comment', method='POST')
def comment_remover(request):
    data = json.loads(request.data)
    comment_id = data.get('comment', None)
    if comment_id:
        command = 'delete from comment where id=?'
        sql_result = app.db.execute_command(command, params=(comment_id,))
        
    return json.dumps({'success': True}), {'Content-Type':'application/json'}


#select first_name, name from comment  inner join city on city.id=city_id where city_id = 8;


# gets area id and return list of cities inside <option> tag
@app.route('/get-cities', method='GET')
def get_cities(request):
    area_id = request.query.get('area', None)
    context={}
    if area_id:
        command = 'select id, name, area_id from city where area_id=?'
        sql_result = app.db.execute_command(command, params=area_id)
        data = []
        for item in sql_result:
            data.append('<option value="' + str(item[0]) + '">' + item[1] + '</option>')
        
        context['cities'] = ''.join(data)
    return json.dumps(context), {'Content-Type':'application/json'}

@app.route('/view/', method='GET')
def comment_view(request):
    context = {}
    data = []
    command = 'select comment.id, first_name, last_name, city.name, comment from comment left join \
        city on city.id=comment.city_id;'
    sql_result = app.db.execute_command(command)
    for item in sql_result:
            data.append('<li><ul><li>' + item[1] +
             '</li><li>' + item[2] +
              '</li><li>' + unicode(item[3]) +
               '</li><li><b>' + item[4] +
               '</b></li></ul><div class="remove" id="' + str(item[0]) + '">Удалить</div></li>')
    
    context['comments'] = ''.join(data)
    context['title'] = 'Comments'
    return context, {}, 'templates/comlist.html'

@app.route('/stat/', method='GET')
def comment_view(request):
    context = {}
    data = []
    comment_min_count = 5
    command = 'select sum(cc), areaname, areapk from( \
        select area.name as areaname, city.area_id as areapk, comcount as cc from city, area, ( \
            select count(comment.city_id) as comcount, city_id as cid from comment group by city_id having city_id) \
                where city.id=cid and area.id=city.area_id) group by areaname having sum(cc) > ?;'
    sql_result = app.db.execute_command(command, params=(comment_min_count,))

    for item in sql_result:
            data.append('<li><a href="/stat-cities?area=' + unicode(item[2]) + '">' +
              item[1] + ' - ' + unicode(item[0]) +'</a></li>')
    
    context['comments'] = ''.join(data)
    context['title'] = 'Comments'
    return context, {}, 'templates/comlist.html'

@app.route('/stat-cities', method='GET')
def stat_cities(request):
    area_id = request.query.get('area', None)
    context={}
    if area_id:
        command = 'select city.name, comcount, city.id as citypk from city, ( \
            select count(comment.city_id) as comcount, city_id as cid from comment group by city_id having city_id) \
                where city.area_id=? and city.id=cid;'
        sql_result = app.db.execute_command(command, params=area_id)
        data = []
        for item in sql_result:
            data.append('<li>' + item[0] + ' - ' + unicode(item[1]) + '</li>')
    
        context['comments'] = ''.join(data)
        context['title'] = 'Comments'
        return context, {}, 'templates/comlist.html'