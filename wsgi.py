# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import cgi
import json
import six
from functools import wraps
from Cookie import SimpleCookie
import time
import httplib
import string

import settings
from db import DBControl

def generate_session_id():
    return

class Request(object):
    """
    Describes request object with environment variables
    """

    def __init__(self, environ):
        self.environ = environ
        self.headers = self._parse_headers(environ)
        self.query = self._parse_query(environ)
        self.data = self._parse_data(environ)
        self.session = self._parse_session(environ)

    def _parse_query(self, environ):
        query = cgi.parse_qs(environ['QUERY_STRING'])
        return {k: v[0] for k, v in query.items()}

    def _parse_session(self, environ):
        cookies = SimpleCookie(environ['HTTP_COOKIE'])
        if cookies:
            try:
                session = cookies[settings.SESSION_COOKIE_NAME]
                return session.value
            except KeyError:
                return None
        else:
            return None


    def _parse_headers(self, environ):
        length = environ.get('CONTENT_LENGTH', 0)
        headers = {'CONTENT_LENGTH': 0 if not length else int(length)}

        wanted_headers = ['REQUEST_METHOD', 'PATH_INFO', 'REMOTE_ADDR',
                          'REMOTE_HOST', 'CONTENT_TYPE', 'COOKIES']

        for k, v in environ.items():
            if k in wanted_headers or k.startswith('HTTP'):
                headers[k] = v
        return headers

    def _parse_data(self, environ):
        try:
            content_type = environ['CONTENT_TYPE'].lower()
        except KeyError:
            content_type = ''
        data = {}
        if 'form' in content_type:
            environ['wsgi.input'].read()
            env_data = cgi.FieldStorage(environ['wsgi.input'],
                                        environ=environ)
            for k in env_data.list:
                # filter out url queries
                if not isinstance(k, cgi.MiniFieldStorage):
                    if k.filename:
                        data[k.name] = k.file
                    else:
                        data[k.name] = k.value
            return data
        else:
            length = self.headers['CONTENT_LENGTH']
            return environ['wsgi.input'].read(length)

# its a little embaressed, but it works =)
# this class, extended from standart Formatted, makes css and js {}
# to be in their places
class PartialFormatter(string.Formatter):
    def get_field(self, field_name, args, kwargs):
        # Handle a key not found
        try:
            val = super(PartialFormatter, self).get_field(field_name, args, kwargs)
        except (KeyError, AttributeError, ValueError):
            val = '{' + field_name + '}', field_name
        return val
    def format_field(self, value, spec):
        try:
            return super(PartialFormatter, self).format_field(value, spec)
        except ValueError:
            # this is only for css
            return str(value.replace('}', '')) + ':' + str(spec) + '}'

class Response(object):
    """
    Response object is responsable for initiating the make_response and returning the view data
    :params code, the status code
    :params data, the raw data rendered from the view

    """
    def __init__(self, start_response, code=200, data='', force_session=False):
        # view can return str or str and a dict of headers
        if isinstance(data, tuple):
            self.data = data[0]
            headers = data[1]
            try:
                template_file = data[2]
            except IndexError:
                template_file = None
        else:
            self.data = data
            headers = {}
            template_file = None
        # here we checking headers to set up content-type and session
        # if they haven't been setted
        if 'content-type' not in map(lambda x: x.lower(), headers):
            headers['Content-Type'] = 'text/html; charset=utf-8'
        
        # if we have a template, parse it
        if template_file:
            if not isinstance(self.data, dict):
                    self.data = {
                    'title': 'template error',
                    'error': 'template using error',
                    'description': 'If you want to use template, make sure your first "response" argument is dict instance and its keys are inside this template',
                    }
                    template_file = 'templates/error.html'
            fmt = PartialFormatter()
            with open(template_file, 'r+') as template:
                js, template = self.removejs(self._force_text(template.read()))
                self.data = fmt.format(template.decode('utf-8'),**self.data) + js.decode('utf-8')

        if force_session:
            session_cookie = SimpleCookie()
            session_cookie[settings.SESSION_COOKIE_NAME] = time.time()
            session_cookie[settings.SESSION_COOKIE_NAME]["Path"] = '/'
            expires = time.time() + settings.SESSION_EXPIRED_TIME * 24 * 3600
            
            session_cookie[settings.SESSION_COOKIE_NAME]["expires"] = time.strftime("%a, %d-%b-%Y %T GMT", time.gmtime(expires))
            headers["Set-Cookie"] =  session_cookie[settings.SESSION_COOKIE_NAME].OutputString()

            

        self.headers = [(self._force_text(k), self._force_text(v)) for k, v in headers.items()]
        self.code = code
        self.start_response = start_response

    def removejs(self, template):
        start = template.find(self._force_text('<script'))
        js = template[start:] + self._force_text('</script></body></html>')
        
        return js, template[:start]

    def _force_text(self, data):
        try:
            data = str(data.encode('utf-8'))
        except UnicodeDecodeError:
            data = str(data.decode('utf-8').encode('utf-8'))
        return data


    def render(self):
        resp_code = self._force_text('{} {}'.format(self.code, httplib.responses[self.code]))

        if str(self.code)[0] in ['4', '5']:
            self.start_response(resp_code, self.headers)
            yield resp_code

        data = self._force_text(self.data)

        self.start_response(resp_code, self.headers)

        yield data



class App(object):
    def __init__(self):
        self.routes = {}
        self.db = DBControl(settings.DB_NAME)
    def route(self, url, method='GET'):
        def decorate(f):
            @wraps(f)
            def wrapper(*args, **kwargs):
                return f(*args, **kwargs)
            self.routes[url] = {'method': method, 'func': wrapper}
            return wrapper
        return decorate

    def path_dispatch(self, request, start_response):
        path = request.headers['PATH_INFO']

        method = request.headers['REQUEST_METHOD']
        view = self.routes.get(path)
        if not view:
            response = Response(start_response, 404)
        elif method != view['method']:
            response = Response(start_response, 405)
        else:
            data = view['func'](request)
            if not request.session:
                response = Response(start_response, data=data, force_session=True)
            else:
                response = Response(start_response, data=data, force_session=True)

            
        return response

    def dispatch_request(self, environ, start_response):
        request = Request(environ)
        response = self.path_dispatch(request, start_response)

        return response

    def __call__(self, environ, start_response):
        """The main wsgi app"""

        response = self.dispatch_request(environ, start_response)

        return response.render()

        


    




















